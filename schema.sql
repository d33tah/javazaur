DROP TABLE Comment;
DROP TABLE Article;
DROP TABLE Account;

CREATE TABLE Account (
    -- http://www.binarytides.com/create-autoincrement-columnfield-in-apache-derby/
    login VARCHAR(100) NOT NULL ,
    password VARCHAR(128),
    CONSTRAINT account_login__pk PRIMARY KEY (login)
);

CREATE TABLE Article (
    id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
    content LONG VARCHAR,
    author VARCHAR(100) NOT NULL,
    title VARCHAR(150),
    add_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT article_id__pk PRIMARY KEY (id),
    CONSTRAINT account_login__fk FOREIGN KEY (author) 
    REFERENCES Account (login)
);

CREATE TABLE Comment (
    id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
    content LONG VARCHAR,
    article_id INTEGER NOT NULL,
    author VARCHAR(100) NOT NULL,
    add_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT comment_id__pk PRIMARY KEY (id),
    CONSTRAINT article_id__fk FOREIGN KEY (article_id) 
    REFERENCES Article (id),
    CONSTRAINT comment_login__fk FOREIGN KEY (author) 
    REFERENCES Account (login)
);

INSERT INTO Account VALUES ('admin', 'a');
INSERT INTO Article (content, author, title) 
    VALUES ('Hello. This is message from admin!', 'admin', 'title for article');
INSERT INTO Comment (content, article_id, author) 
    VALUES ('Comment', 1, 'admin');