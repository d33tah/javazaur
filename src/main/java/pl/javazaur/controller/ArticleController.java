package pl.javazaur.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import pl.javazaur.dao.AccountDao;
import pl.javazaur.dao.ArticleDao;
import pl.javazaur.pojo.Account;
import pl.javazaur.pojo.Article;

@Controller
@Transactional
@SessionAttributes
public class ArticleController {

    @Autowired
    private ArticleDao articleDao;
    @Autowired
    private AccountDao accountDao;

    //## INDEX | DETAILS SECTION ##\\
    @RequestMapping(value = "/article/*")
    public ModelAndView list() {
        return new ModelAndView("article/index", "articles", articleDao.getArticleList());
    }

    @RequestMapping(value = "/article/details/{articleId}**")
    public ModelAndView details(@PathVariable("articleId") int articleId) {

        return new ModelAndView("article/details", "articleId", articleId);
    }

    //## ADD SECTION ##\\
    @RequestMapping(value = "/article/add", method = RequestMethod.GET)
    public ModelAndView add() {
        return new ModelAndView("article/add", "command", new Article());
    }

    @RequestMapping(value = "/article/add", method = RequestMethod.POST)
    public String addArticle(
            @ModelAttribute("article") Article article,
            BindingResult result) {

        if (article.getContent().isEmpty() || article.getTitle().isEmpty()) {
            return "redirect:/article/add?error=1";
        }

//        Get user from spring security
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String login = user.getUsername();
        Account author = accountDao.getAccount(login);

        article.setAuthor(author);

        articleDao.addArticle(article);

        return "redirect:index.htm";
    }

    //## EDIT SECTION ##\\
    @RequestMapping("/article/edit")
    public ModelAndView edit(HttpServletRequest request) {
        int id = Integer.parseInt(request.getParameter("id"));

        return new ModelAndView("edit", "editedArticle", articleDao.getArticle(id));
    }

    @RequestMapping(value = "/article/edit/{articleId}**", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable("articleId") int articleId) {
        return new ModelAndView("article/edit", "editedArticle", articleDao.getArticle(articleId));
    }

    @RequestMapping(value = "/article/edit/{articleId}**", method = RequestMethod.POST)
    public ModelAndView editArticle(@PathVariable("articleId") int articleId,
            @ModelAttribute("article") Article article,
            BindingResult result) {

        String redirection = "redirect:/article/edit/" + articleId + "?error=1";
        if (article.getContent().isEmpty() || article.getTitle().isEmpty()) {
            return new ModelAndView(redirection, "editedArticle", article);
        }

        articleDao.updateArticle(articleId, article.getContent());

        redirection = "redirect:/article/details/" + articleId;
        return new ModelAndView(redirection);
    }

    //## DELETE SECTION ##\\
    @RequestMapping("/article/delete/{articleId}**")
    public ModelAndView delete(@PathVariable("articleId") int articleId) {

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String login = user.getUsername();

        String redirection;
        
        Article article = articleDao.getArticle(articleId);
        
        if (!login.equals(article.getAuthor().getLogin()))
            return new ModelAndView("article/delete_error");
            
        articleDao.deleteArticle(articleId);

        redirection = "redirect:/article/";
        
        return new ModelAndView(redirection);       
    }
}
