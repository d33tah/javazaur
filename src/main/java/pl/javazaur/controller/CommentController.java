package pl.javazaur.controller;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import pl.javazaur.dao.AccountDao;
import pl.javazaur.dao.ArticleDao;
import pl.javazaur.dao.CommentDao;
import pl.javazaur.pojo.Account;
import pl.javazaur.pojo.Article;
import pl.javazaur.pojo.Comment;
/**
 *
 * @author zico
 */
@Controller
@Transactional
@SessionAttributes
public class CommentController {
    
    @Autowired
    private CommentDao commentDao;
    
    @Autowired
    private ArticleDao articleDao;
    
    @Autowired
    private AccountDao accountDao;
    
    @RequestMapping(value = "/comment/add/{articleId}**", method = RequestMethod.GET)
    public ModelAndView add(@PathVariable("articleId") int articleId) {
        return new ModelAndView("comment/add", "articleId", articleId);
    }
    
    @RequestMapping(value = "/comment/add/{articleId}**", method = RequestMethod.POST)
    public String addComment(@PathVariable("articleId") int articleId,
            @ModelAttribute("comment") Comment comment,
            BindingResult result) {
        
        if (comment.getContent().isEmpty())
            return ("redirect:/comment/add/" + articleId + "?error=1");
        
        Article article = articleDao.getArticle(articleId);
        
        if (article == null)
            return "redirect:/index.htm";
        
//        Get user from spring security
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String login = user.getUsername();
        Account author = accountDao.getAccount(login);
        
        comment.setArticleId(article);
        comment.setAuthor(author);
        comment.setAddDate(new Date());
        
        commentDao.addComment(comment);

        return "redirect:/index.htm";
    }
}
