package pl.javazaur.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import pl.javazaur.dao.AccountDao;

import org.apache.log4j.Logger;

@Controller
public class IndexController {

    @Autowired
    private AccountDao accountDao;
    
    private static final Logger log = Logger.getRootLogger();
    
    @RequestMapping({"/", "/index"})
    public ModelAndView list() {
        log.info("log4j test: IndexController.list()");
        return new ModelAndView("index", "accounts", accountDao.getAccountList());
    }

    @RequestMapping({"/login"})
    public ModelAndView login() {
        log.info("Login");
        return new ModelAndView("redirect:/");
    }
}
