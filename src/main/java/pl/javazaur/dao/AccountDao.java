package pl.javazaur.dao;

import java.util.List;
import pl.javazaur.pojo.Account;

public interface AccountDao {

    List<Account> getAccountList();

    Account getAccount(String login);

    void addAccount(Account account);

    void deleteAccount(String login);

    void updateAccount(String login, String password);

}
