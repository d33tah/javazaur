package pl.javazaur.dao.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.javazaur.dao.ArticleDao;
import pl.javazaur.pojo.Account;
import pl.javazaur.pojo.Article;

@Repository
@Transactional
public class ArticleDaoImpl implements ArticleDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Article> getArticleList() {
        return sessionFactory.getCurrentSession().createQuery("from Article order by add_date DESC").list();
    }

    @Override
    public Article getArticle(int id) {
        return (Article) sessionFactory.getCurrentSession().get(Article.class, id);
    }

    @Override
    public void addArticle(Article article) {
        article.setAddDate(new Date());
        sessionFactory.getCurrentSession().save(article);
    }

    @Override
    public void deleteArticle(int id) {
        Article article = (Article) sessionFactory.getCurrentSession().get(Article.class, id);
        sessionFactory.getCurrentSession().delete(article);
    }

    @Override
    public void updateArticle(int id, String content) {
        Article article = (Article) sessionFactory.getCurrentSession().get(Article.class, id);
        article.setContent(content);
        article.setAddDate(new Date());
        sessionFactory.getCurrentSession().update(article);
    }
}
