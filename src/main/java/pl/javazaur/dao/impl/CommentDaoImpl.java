package pl.javazaur.dao.impl;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.javazaur.dao.CommentDao;
import pl.javazaur.pojo.Account;
import pl.javazaur.pojo.Article;
import pl.javazaur.pojo.Comment;
/**
 *
 * @author zico
 */

@Repository
@Transactional

public class CommentDaoImpl implements CommentDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Comment> getCommentList() {
        return sessionFactory.getCurrentSession().createQuery("FROM Comment").list();
    }

    @Override
    public Comment getComment(int id) {
        return (Comment) sessionFactory.getCurrentSession().get(Comment.class, id);
    }

    @Override
    public List<Comment> getCommentsForArticle(int articleId) {
        String query = "FROM Comment WHERE Article_Id= :articleId";
        return sessionFactory.getCurrentSession().createQuery(query).setParameter("articleId", articleId).list();
        
    }

    @Override
    public void addComment(Comment comment) {
        sessionFactory.getCurrentSession().save(comment);
    }

    @Override
    public void deleteComment(int id) {
        Comment comment = (Comment) sessionFactory.getCurrentSession().get(Comment.class, id);
        sessionFactory.getCurrentSession().delete(comment);
    }

    @Override
    public void editComment(int id, String content) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
