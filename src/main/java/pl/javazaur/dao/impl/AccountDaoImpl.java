package pl.javazaur.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.javazaur.dao.AccountDao;
import pl.javazaur.pojo.Account;

@Repository
@Transactional
public class AccountDaoImpl implements AccountDao, UserDetailsService {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Account> getAccountList() {
        return sessionFactory.getCurrentSession().createQuery("from Account").list();
    }

    @Override
    public Account getAccount(String login) {
        return (Account) sessionFactory.getCurrentSession().get(Account.class, login);
    }

    @Override
    public void addAccount(Account account) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteAccount(String account) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateAccount(String login, String password) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public UserDetails loadUserByUsername(String string) throws UsernameNotFoundException {
        Account acc = this.getAccount(string);
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        authorities.add(
                new GrantedAuthorityImpl("ROLE_USER"));

        return new User(acc.getLogin(), acc.getPassword(), true, true, true, true, authorities);
    }

}
