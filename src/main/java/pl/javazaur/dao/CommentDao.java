package pl.javazaur.dao;
import java.util.List;
import pl.javazaur.pojo.Account;
import pl.javazaur.pojo.Comment;
/**
 *
 * @author zico
 */
public interface CommentDao {
    
    List<Comment> getCommentList();
    Comment getComment(int id);
    List<Comment> getCommentsForArticle(int article_id);
    
    void addComment(Comment comment);
    void deleteComment(int id);
    void editComment(int id, String content);
    
}