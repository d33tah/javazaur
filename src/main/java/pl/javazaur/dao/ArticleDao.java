package pl.javazaur.dao;

import java.util.List;
import pl.javazaur.pojo.Account;
import pl.javazaur.pojo.Article;

public interface ArticleDao {

    List<Article> getArticleList();

    Article getArticle(int id);

    void addArticle(Article article);

    void deleteArticle(int id);

    void updateArticle(int id, String content);

}
