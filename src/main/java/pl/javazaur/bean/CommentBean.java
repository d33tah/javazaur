/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.javazaur.bean;

import java.util.List;
import javax.faces.bean.ManagedBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import pl.javazaur.dao.CommentDao;
import pl.javazaur.pojo.Comment;

/**
 *
 * @author zico
 */
@Scope("session")
@ManagedBean
@Controller
public class CommentBean {

    @Autowired
    private CommentDao commentDao;

    private String author;
    private String content;

    public String getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Comment> getAllComments() {
        return commentDao.getCommentList();
    }
    
    public List<Comment> getCommentsForArticle(int articleId) {
        return commentDao.getCommentsForArticle(articleId);
    }

    public String addArticle() {

        Comment comment = new Comment();
        comment.setContent(content);

        commentDao.addComment(comment);

        clearForm();

        return "";
    }

    //clear form values
    private void clearForm() {
        setContent("");
    }

    /**
     * Creates a new instance of ArticleBean
     */
    public CommentBean() {
        
    }
}
