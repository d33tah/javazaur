/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.javazaur.bean;

import java.io.IOException;
import java.util.List;
import javax.faces.FacesException;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.validation.constraints.Size;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import pl.javazaur.dao.AccountDao;
import pl.javazaur.dao.ArticleDao;
import pl.javazaur.pojo.Article;
import org.apache.log4j.Logger;

/**
 *
 * @author zico
 */
@Controller
@Scope("session")
@ManagedBean(name = "articleBean")
public class ArticleBean {

    //get log4j handler
    private static final Logger logger = Logger.getLogger(ArticleBean.class);
    
    @Autowired
    private ArticleDao articleDao;

    @Autowired
    private AccountDao accountDao;

    @Size(min = 1)
    private String content;
    @Size(min = 1, max = 120)
    private String title;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String message;

    public String getMessage() {
        return message;
    }

    public Article getArticle(int id) {
        logger.debug("getArticle(" + id +")");
        Article article = articleDao.getArticle(id);
        if (article != null) {
            logger.debug("title: " + article.getTitle());
        }
        return article;
    }

    public List<Article> getAllArticles() {
        logger.debug("getAllArticles");
        return articleDao.getArticleList();
    }

    //clear form values
    private void clearForm() {
        setContent("");
        setTitle("");
    }

    /**
     * Creates a new instance of ArticleBean
     */
    public ArticleBean() {
        logger.debug("ArticleBean");
        clearForm();
    }
}
