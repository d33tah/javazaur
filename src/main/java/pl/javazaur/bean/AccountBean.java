/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.javazaur.bean;

import javax.faces.bean.ManagedBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import pl.javazaur.dao.AccountDao;

/**
 *
 * @author zico
 */
@Controller
@Scope("session")
@ManagedBean(name = "accountBean")
public class AccountBean {

    @Autowired
    private AccountDao accountDao;

    private String userName;
    
    /**
     * Creates a new instance of AccountBean
     */
    public AccountBean() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        userName = user.getUsername();
    }
    
    public String getUserName() {
        return userName;
    }

    public boolean CheckIsLogged() {
        return SecurityContextHolder.getContext().getAuthentication() != null && SecurityContextHolder.getContext().getAuthentication().isAuthenticated();
    }
}
